# SVV Material Lab 6

This lab is divided into two sessions:

## Session 1: Development and assessment of test suites

In this session, the student has to develop a test suite for a program under test. At the end of the session, the test suite will be  evaluated against a set of  assessment criteria regarding the quality of the developed test suite. 

### Files for this session

- [PDF file](https://gitlab.com/ucase/public/svv-material-lab-6/-/blob/master/VVS-Lab6-S1.pdf), with the description of this session.
- [Zip file](https://gitlab.com/ucase/public/svv-material-lab-6/-/blob/master/Material-S1.zip), with the files required to complete this session.

## Session 2: Evaluation applying mutation testing with MuCPP

In this session, the test cases designed in the first part of this lab will be evaluated based on the mutation adequacy criterion. This will be done by using the [MuCPP mutation tool](https://ucase.uca.es/mucpp/).

### Files for this session

- [PDF file](https://gitlab.com/ucase/public/svv-material-lab-6/-/blob/master/VVS-Lab6-S2.pdf), with the description of this session.
- [Zip file](https://gitlab.com/ucase/public/svv-material-lab-6/-/blob/master/Material-S2.zip), with the files required to complete this session.

_The source code files family.[cpp|hpp] have been adapted from a listing in [1]. Note that, while considered to be fault free in this lab, these source files are used in other sessions where students are expected to detect some defects in them._


> [1] S.  Wiener  and  L.  J.  Pinson, The  C++  Workbook. USA:  Addison-Wesley Longman Publishing Co., Inc., 1990.
